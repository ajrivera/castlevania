## Controls

- Standard Controls WASD.
- E for interaction.
- Space to do melee attack.
- J to reduce own hp.
- L to change to next level.

## Singleton

My own main character hero is the Singleton.

## Interface

It's called killable and it specifies what can be killed by our protagonist.

## 2nd Level

You can use the shortcut or go straight for the altar/shrine to get teleported to it.

This is were all the enemies are implemented

## Additionals

I did a small AI for my enemies to go collide with the protagonist if its in their sights.

Implemented an drops system for my enemies but still didn't do the main character inventory so its useless.

Tried doing a Window Singleton but althought it worked it left some flashing sprite that was too annoying to keep using.

Now there's an HUD where u can see the hp u have and a green bar that reflects it.

To progress across the 2nd level you will have to use levers to open doors.

There are some events that happen when you interact or walk somewhere.

Now the 2 maps are implemented by putting an sprite that is the whole map then putting the terrain with invisible sprites.(Efficiency++).