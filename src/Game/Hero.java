package Game;

import Game.Core.Field;
import Game.Core.Sprite;

public class Hero extends Sprite {

	private static Hero heroS = null;

	/**
	 * velocitat del personatge
	 */
	double spd;
	int atk;
	int hp;
	int maxhp;
	public static boolean visionr = true;

	private Hero(String name, int x1, int y1, int x2, int y2, double spdd, int atk, int maxhp, String path) {
		super(name, x1, y1, x2, y2, path);
		this.physicBody = true;
		this.trigger = false;
//		this.unscrollable = true;
		hp = maxhp;
		spd = spdd;
		this.atk = atk;
		this.maxhp = maxhp;
	}

	public static Hero getInstance(String name, int x1, int y1, int x2, int y2, double spdd, int atk, int maxhp,
			String path) {
		if (heroS == null) {
			heroS = new Hero(name, x1, y1, x2, y2, spdd, atk, maxhp, path);

		}

		return heroS;
	}

	/**
	 * limitador boolean per a no fer multiples salts al aire.
	 */
	public static boolean aterra = true; // 0 en terra, 1 pujant 2 caient
	// int jumpdistance;
//	boolean useArrayImages = false;

	/**
	 * maquina de estados 1 movimiento a la derecha
	 */
	public void moveRight() {
		// x1+=3;
		// x2+=3;
//		 if(aterra) {
//		System.out.println("intento r " + velocity[0] + " " + velocity[1]);
		this.setVelocity(spd, velocity[1]);
//		Castle.heroS.addVelocity(spd, velocity[1]);
		visionr = true;
//		x1 += spd;
//		x2 += spd;
//		 }

	}

	/**
	 * maquina de estados 2 movimiento a la izquierda
	 */
	public void moveLeft() {
		// x1-=3;
		// x2-=3;
		// if(aterra) {
//		System.out.println("intento l " + velocity[0] + " " + velocity[1]);
		this.setVelocity(-spd, velocity[1]);
//		Castle.heroS.addVelocity(-spd, velocity[1]);
		visionr = false;
//		x1 -= spd;
//		x2 -= spd;
		// }

	}

	/**
	 * maquina de estados 3 cuando no se mueve el personaje
	 */
	public void doNotMove() {
		// TODO Auto-generated method stub
		// System.out.println("PARADO PERO NO EN EL SUELO");
		if (aterra) {
			// System.out.println("PARADO");
			heroS.setVelocity(0, velocity[1]);
		}
		// this.changeImage("");

	}

	/**
	 * aplica fuerza hacia arriba al personaje para simular un salto
	 */
	public void jump() {
		// TODO Auto-generated method stub

		if (aterra && heroS.force[1] >= 0) {
//			heroS.setVelocity(heroS.velocity[0], 0);
//			heroS.addForce(0, -1.95d);

			heroS.setForce(this.force[0], -1.90d);

//			heroS.setForce(this.force[0], -2.22d);
			aterra = false;
			// System.out.println("salto");
		}

	}

	public void gravity() {
		// TODO Auto-generated method stub
		heroS.setConstantForce(0, 0.26);

	}
	/**
	 * ataque a melee del hero, comprueba antes hacia que lado esta mirando con la bool visionr
	 * @return
	 */
	public Hitbox Melee() {
		if (visionr) {
			Hitbox h = new Hitbox("hitbox", x2, (y1 + y2) / 2, x2 + 50, ((y1 + y2) / 2) + 50, 0);
			System.out.println("rightHIT");
			return h;
		} else {
			Hitbox h = new Hitbox("hitbox", x1 - 50, (y1 + y2) / 2, x1, ((y1 + y2) / 2) + 50, 0);
			System.out.println("leftHIT");
			return h;
		}

	}

	/**
	 * no funciona pero en algun momento funcionara
	 * 
	 * @return
	 */
	public Shoot shoot() {
		// TODO Auto-generated method stub
		Shoot s = new Shoot("shoot", x1 + 90, y1, x2 + 90, y2, 0, "rayo.gif");

		return s;

	}

}
