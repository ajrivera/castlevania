package Game;

import java.util.ArrayList;

import Game.Core.*;

public interface Killable {

	public default void dead(Enemy e, ArrayList<Sprite> mainlist) {
		// TODO Auto-generated method stub
		if (e.hp <= 0) {
			e.delete();
			mainlist.remove(e);
		}

	}
}
