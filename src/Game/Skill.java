package Game;

import Game.Core.Sprite;

public class Skill extends Sprite {

	int rango;
	int cooldown;
	int manaE;

	public Skill(String name, int x1, int y1, int x2, int y2, int rango, int cooldown, int manaE, String path) {
		super(name, x1, y1, x2, y2, path);
		this.rango = rango;
		this.cooldown = cooldown;
		this.manaE = manaE;
	}

}
