package Game;

import Game.Core.Sprite;

public class backgroundblock extends Sprite{
	public backgroundblock(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		terrain = false;
		trigger = false;
		physicBody = false;
		solid = false;
	}
}
