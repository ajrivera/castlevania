package Game;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashSet;

import Game.Core.Field;
import Game.Core.Sprite;
import Game.Core.Window;

public class Castle2 {

	static Hero heroS;

	// WINDOW
	static Field f = new Field();
	static Window w = new Window(f);
//	static Window w = WindowSingleton.getWindowSingleton(f);

	static interact chest = new interact("chest", 505, 391, 563, 443, 0, "IMG/interact/ChestOFF.png");
//	static interact chest2 = new interact("chest2", 1246, 455, 1304, 507, 0, "IMG/interact/ChestOFF.png");
	static interact altar = new interact("altar", 1246, 465, 1304, 507, 0, "IMG/interact/AltarIdle.gif");
	static interact lever1 = new interact("lever1", 1090, 395, 1148, 446, 0, "IMG/interact/LeverOFF.png");
	static interact lever2 = new interact("lever2", 963, 892, 1021, 943, 0, "IMG/interact/LeverOFF.png");
	static interact lever3 = new interact("lever3", 233, 852, 291, 903, 0, "IMG/interact/LeverOFF.png");

	static platf1 platm1 = new platf1("platm1", 467, 144, 533, 152, "IMG/interact/Platform.png");
	static platf1 platm2 = new platf1("platm2", 557, 144, 689, 152, "IMG/interact/Platform.png");
	static platf1 platm3 = new platf1("platm3", 713, 144, 844, 152, "IMG/interact/Platform.png");
	static platf1 platm4 = new platf1("platm4", 869, 144, 934, 152, "IMG/interact/Platform.png");

	static platf1 platDoor1 = new platf1("platDoor1", 1150, 216, 1172, 306, "IMG/interact/Door.png");
	static platf1 platDoor2 = new platf1("platDoor2", 446, 870, 469, 960, "IMG/interact/Door.png");
	static platf1 platDoor3 = new platf1("platDoor3", 1168, 870, 1190, 959, "IMG/interact/Door.png");

	static Swordsman S1 = new Swordsman("S1", 480, 110, 513, 146, 1, 10, 100, "IMG/enemigos/goblin/GoblinIdle.gif");
	static Swordsman S2 = new Swordsman("S2", 567, 110, 605, 140, 1, 10, 100, "IMG/enemigos/goblin/GoblinIdle.gif");
	static Swordsman S3 = new Swordsman("S3", 636, 110, 674, 140, 1.5, 10, 100, "IMG/enemigos/goblin/GoblinIdle.gif");
	static Swordsman S4 = new Swordsman("S4", 727, 110, 752, 140, 1.5, 10, 100,
			"IMG/enemigos/goblin/GoblinIdleMirr.gif");
	static Swordsman S5 = new Swordsman("S5", 797, 110, 833, 140, 1, 10, 100, "IMG/enemigos/goblin/GoblinIdleMirr.gif");
	static Swordsman S6 = new Swordsman("S6", 880, 110, 910, 140, 2, 10, 100, "IMG/enemigos/goblin/GoblinIdle.gif");

	static Shielded Sh1 = new Shielded("Sh1", 858, 660, 903, 711, 1, 10, 100, "IMG/enemigos/skeleton/SkeletonIdle.gif");
	static Shielded Sh2 = new Shielded("Sh2", 1106, 924, 1143, 956, 1, 10, 100,
			"IMG/enemigos/skeleton/SkeletonIdleMirr.gif");
	static Shielded Sh3 = new Shielded("Sh3", 833, 917, 878, 954, 1, 10, 100,
			"IMG/enemigos/skeleton/SkeletonIdleMirr.gif");
	static Shielded Sh4 = new Shielded("Sh4", 687, 914, 737, 954, 1, 10, 100,
			"IMG/enemigos/skeleton/SkeletonIdleMirr.gif");
	static Shielded Sh5 = new Shielded("Sh5", 285, 865, 315, 897, 1, 10, 100, "IMG/enemigos/skeleton/SkeletonIdle.gif");

//	static Texto t2 = new Texto("t2", 50, 50, 100, 100, "T2prueba");
	static int minscrollx = 200;
	static int minscrolly = 600;
	static int maxscrollx = 500;
	static int maxscrolly = 900;

	static int door1 = 2000;
	static int door2 = 2000;
	static int door3 = 2000;
	static mapa2 mapa2 = new mapa2();
	// INTERACTION BOOLS
	static boolean cofre1;
	static boolean altar1;
	static boolean event1;

	// STARTING POINT

	static int var1 = 900;
	static int stx1 = 230;
	static int sty1 = 319;
	static int stx2 = 283;
	static int sty2 = 380;
	static int timer = 0;
	static ArrayList<Sprite> mainD = new ArrayList<>();
	static ArrayList<interact> platinter = new ArrayList<>();
	static ArrayList<Sprite> platevent = new ArrayList<>();
	static ArrayList<Enemy> enemigos = new ArrayList<>();
	public static ArrayList<Hitbox> attacksHit = new ArrayList<Hitbox>();

	public static void main(String[] args) throws InterruptedException {

		heroS = Hero.getInstance("hero", 230, 319, 283, 380, 3.5, 30, 200, "IMG/PJCORR/untitled.gif");
		Texto t1 = new Texto("t1", 50, 0, 100, 50, "Vida " + heroS.maxhp + " / " + heroS.hp);
		Color colhp = new Color(0, 255, 0, 155);
		ColorSprite hpbar = new ColorSprite("hpbar", 155, 36, heroS.maxhp + 155, 50, 0, colhp);

		heroS.x1 = stx1;
		heroS.y1 = sty1;
		heroS.x2 = stx2;
		heroS.y2 = sty2;
		long last_time = System.currentTimeMillis();
		stx1 = heroS.x1;
		sty1 = heroS.y1;
		stx2 = heroS.x2;
		sty2 = heroS.y2;
//		int[] arrst = {stx1, sty1, stx2, sty2};
		// plataformas.add(platf1);
		// plataformas.add(platf2);
//		System.out.println(Swordsman.getDrops());
		// hero.setConstantForce(0, 0.2);
		// hero.MARGIN=0;

		backgroundblock top2 = new backgroundblock("top2", 0, 0, 1920, 1080, "IMG/2LVL/Level02TOP.png");
		backgroundblock back2 = new backgroundblock("top2", 0, 0, 1920, 1080, "IMG/2LVL/Level02BACK.png");

		mapa2.createmap2();

//		f.background = "black.png";
		mainD.add(back2);

		platinter.add(chest);
//		platinter.add(chest2);
		platinter.add(altar);
		platinter.add(lever1);
		mainD.add(platDoor1);
		platinter.add(lever2);
		mainD.add(platDoor2);
		platinter.add(lever3);
		mainD.add(platDoor3);
		mainD.addAll(platinter);

		enemigos.add(S1);
		enemigos.add(S2);
		enemigos.add(S3);
		enemigos.add(S4);
		enemigos.add(S5);
		enemigos.add(S6);

		enemigos.add(Sh1);
		enemigos.add(Sh2);
		enemigos.add(Sh3);
		enemigos.add(Sh4);
		enemigos.add(Sh5);
		// mainD.add(E1);
		mainD.add(heroS);
		mainD.addAll(enemigos);
//		mainD.addAll(attacksHit);

		mainD.add(top2);

		platevent.add(platm1);
		platevent.add(platm2);
		platevent.add(platm3);
		platevent.add(platm4);
		mainD.addAll(platevent);

		mainD.addAll(mapa2.plataformas);
		mainD.addAll(mapa2.AV);
		// HUD
		mainD.add(hpbar);
		mainD.add(t1);
		// starting window (No se utilizar el scroll)

//		f.scroll(0, -400);

		while (true) {
			long time = System.currentTimeMillis();
//			System.out.println(E1.hp);
//			System.out.println(attacksHit.size()+"ATTACKSHIT");
//			System.out.println(mainD.size()+"MAINDDDD");
			if (heroS.collidesWithList(mapa2.AV).size() > 0) {
				instantdeath();
			}
			if (heroS.hp < 0) {
				instantdeath();
				heroS.hp = heroS.maxhp;

			}
			hpbar.x2 = heroS.hp + 155;
			t1.path = "Vida " + heroS.maxhp + " / " + heroS.hp;

			// EVENTOS
			if (heroS.x2 >= 700) {
				platm3.delete();
				event1 = true;
			}
			if (chest.open == 1) {

			}
			if (lever1.open == 1) {
			}

			for (Hitbox h : attacksHit) {
				h.frame();
				if (h.frames == 0) {
//					attacksHit.clear();
				}
			}

			for (Enemy e : enemigos) {
				behaviour(e);
				e.dead(e, mainD);
				if (e.name == "Sh1" || e.name == "Sh2" || e.name == "Sh3" || e.name == "Sh4" || e.name == "Sh5") {
					((Shielded) e).frameInv();
				}
//				if(e.hp<=0) {
//					e.delete();
//					mainD.remove(e);
//				}
			}

			// GRAVITY
			if (heroS.velocity[1] == 0 && heroS.isGrounded(f)) {
//				System.out.println("aaaa");
				heroS.aterra = true;
			} else {
				heroS.gravity();
				for (Enemy e : enemigos) {
					e.setConstantForce(0, 0.26);
				}
			}
			input();

			interactions();

			f.draw(mainD);
			int delta_time = (int) (time - last_time);
			last_time = time;
			// 60fps
			Thread.sleep(20);
//			System.out.println(Thread.activeCount());
//			System.out.println(doorA);
			if (door1 > 0 && lever1.open == 1) {
				door1 = delay(door1, delta_time);
				platDoor1.y1++;
				platDoor1.y2++;
			}
			if (door2 > 0 && lever2.open == 1) {
				door2 = delay(door2, delta_time);
				platDoor2.y1++;
				platDoor2.y2++;
			}
			if (door3 > 0 && lever3.open == 1) {
				door3 = delay(door3, delta_time);
				platDoor3.y1++;
				platDoor3.y2++;
			}
			// mirar pixel dentro del field con el click del raton
			if (f.getCurrentMouseX() != -1) {
				System.out.println(f.getMouseX() + " " + f.getMouseY());
			}

		}

	}

	private static void behaviour(Enemy e) { // TODO Auto-generated method stub

		if (e.x1 >= heroS.x2 && ((e.y2 <= heroS.y2 && e.y2 >= heroS.y1) || (e.y1 <= heroS.y2 && e.y1 >= heroS.y1))) {
			e.moveLeftE();
//			System.out.println("MOVELEFT");

		} else if (e.x2 <= heroS.x1
				&& ((e.y2 <= heroS.y2 && e.y2 >= heroS.y1) || (e.y1 <= heroS.y2 && e.y1 >= heroS.y1))) {
			e.moveRightE();
//			System.out.println("MOVERIGHT");
		} else {
			e.doNotMove();
//			System.out.println("TUSMUERTOS");
		}
	}

	/**
	 * Funcio per a retornar al personatge a un punt de inici quan colisioni amb
	 * instant death sprites com els spikes
	 * 
	 * @param Hero
	 * @param sty2
	 * @param stx2
	 * @param sty1
	 * @param stx1
	 */
	private static void instantdeath() {
		// TODO Auto-generated method stub

//		if (heroS.collidesWithList(mapa2.AV).size() > 0) {
		heroS.x1 = stx1;
		heroS.y1 = sty1;
		heroS.x2 = stx2;
		heroS.y2 = sty2;
//			if (altar.open != 1) {
//				f.resetScroll();
//				f.scroll(0, -400);
//			} else {
//				f.resetScroll();
//				f.scroll(-1000, -400);
//			}
//		}

	}

	/**
	 * Funcio on estan tots els ifs de les interaccions amb sprites tipus cofres o
	 * altars.
	 * 
	 * @param sty2
	 * @param stx2
	 * @param sty1
	 * @param stx1
	 * @param Hero
	 * @return
	 */
	private static void interactions() {
		// TODO Auto-generated method stub
		if (heroS.collidesWith(platinter.get(0)) && w.getPressedKeys().contains('e') && platinter.get(0).open == 0) {
			platinter.get(0).open = 1;
			platinter.get(0).changeImage("IMG/interact/ChestON.png");
			platm2.delete();
			cofre1 = true;
			// System.out.println("firsttru");
			// mapa.sprites.add(buff);
		}
		if (heroS.collidesWith(platinter.get(1)) && w.getPressedKeys().contains('e') && platinter.get(1).open == 0) {
			platinter.get(1).open = 1;
			altar1 = true;
			stx1 = heroS.x1;
			sty1 = heroS.y1;
			stx2 = heroS.x2;
			sty2 = heroS.y2;
			platinter.get(1).changeImage("IMG/interact/AltarActive.gif");
		}
		if (heroS.collidesWith(platinter.get(2)) && w.getPressedKeys().contains('e') && platinter.get(2).open == 0) {
			platinter.get(2).open = 1;
			platinter.get(2).changeImage("IMG/interact/LeverON.png");
			platm1.delete();
			platm4.delete();
			// System.out.println("firsttru");
			// mapa.sprites.add(buff);
		}
		if (heroS.collidesWith(platinter.get(3)) && w.getPressedKeys().contains('e') && platinter.get(3).open == 0) {
			platinter.get(3).open = 1;
			platinter.get(3).changeImage("IMG/interact/LeverON.png");
			// System.out.println("firsttru");
			// mapa.sprites.add(buff);
		}
		if (heroS.collidesWith(platinter.get(4)) && w.getPressedKeys().contains('e') && platinter.get(4).open == 0) {
			platinter.get(4).open = 1;
			platinter.get(4).changeImage("IMG/interact/LeverON.png");
			// System.out.println("firsttru");
			// mapa.sprites.add(buff);
		}
	}

	private static int delay(int a, int b) {
		// TODO Auto-generated method stub

		a = a - b;

		return a;
	}

	private static void input() throws InterruptedException {

		HashSet<Character> list = (HashSet<Character>) w.getPressedKeys();

		// System.out.println(list);

		if (list.contains('d')) {
			if (heroS.velocity[0] > 0) {
//				f.scroll((int)-heroS.spd, 0);
//				minscrollx -= heroS.spd;
//				maxscrollx -= heroS.spd;
			}
			heroS.moveRight();
		}
		if (list.contains('a')) {
			if (heroS.velocity[0] < 0) {
//				f.scroll((int)heroS.spd, 0);
//				minscrollx += heroS.spd;
//				maxscrollx += heroS.spd;
			}
			heroS.moveLeft();
		}

//		for (int i = 0; i < mainD.size(); i++) {
//			if (mainD.get(i).terrain) {
//				if (heroS.rightOn(mainD.get(i))) {
//					f.scroll((int)heroS.spd, 0);
//					heroS.x1--;
//					heroS.x2--;
//				} else if (heroS.leftOn(mainD.get(i))) {
//					f.scroll((int)-heroS.spd, 0);
//					heroS.x1++;
//					heroS.x2++;
//				}
//			}
//		}

		if (list.contains('w')) {
			heroS.jump();
		}
		if (!w.getPressedKeys().contains('d') && !w.getPressedKeys().contains('a') && heroS.aterra != false) {
			heroS.doNotMove();

		}
		if (w.getKeysDown().contains(' ')) {
			Hitbox h = heroS.Melee();
			for (Enemy e : enemigos) {
				h.colision(e);
			}
			attacksHit.add(h);

		}
		/*
		 * else if(s.size()>0){ s.remove(s.size()-1); }
		 */
		if (list.contains('v')) {
			f.scroll(10, 0);
		}
		if (list.contains('b')) {
			f.scroll(-10, 0);
		}
		if (list.contains('n')) {
			f.scroll(0, 10);
		}
		if (list.contains('m')) {
			f.scroll(0, -10);
		}

		if (list.contains('j')) {
			System.out.println("dolor" + heroS.hp);
			heroS.hp -= 10;
		}
	}
}
