package Game;

import Game.Core.Sprite;
/**
 * clase abstracta para generalizar a los enemigos
 * @author Derek
 *
 */
public abstract class Enemy extends Sprite implements Killable {
	int hp;
	int maxhp;
	double spde;
	int atk;
	boolean aterraE = true;
	boolean shield = false;

	public Enemy(String name, int x1, int y1, int x2, int y2, double spdde, int atk, int maxhp, String path) {
		super(name, x1, y1, x2, y2, path);
		spde = spdde;
		this.hp = maxhp;
		this.maxhp = maxhp;
		this.physicBody = true;
		this.trigger = false;
	}

	public void moveRightE() {
//		 x1+=3;
//		 x2+=3;
//		if (aterraE /*&& Castle.heroS.y1>=this.y2*/) {
		this.setVelocity(spde, velocity[1]);
//		System.out.println("right");
//		}
	}

	public void moveLeftE() {
//		 x1-=3;
//		 x2-=3;
//		if (aterraE /*&& Castle.heroS.y1>=this.y2*/) {
		this.setVelocity(-spde, velocity[1]);
//		System.out.println("left");
//		}
	}

	public void doNotMove() {
		// TODO Auto-generated method stub
		// System.out.println("PARADO PERO NO EN EL SUELO");
//		if (aterra) {
		// System.out.println("PARADO");
		this.setVelocity(0, velocity[1]);
//		}
		// this.changeImage("");

	}
}
