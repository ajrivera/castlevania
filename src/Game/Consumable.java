package Game;

public class Consumable extends Items {

	int uses;
	int pwr;
	public Consumable(String name, int uses, int pwr) {
		super(name);
		this.uses = uses;
		this.pwr = pwr;
		// TODO Auto-generated constructor stub
	}

	static Consumable LowHPpot = new Consumable("LowHPpot",3,10);
	static Consumable MedHPpot = new Consumable("MedHPpot",2,20);
	static Consumable HiHPpot = new Consumable("HiHPpot",1,30);
	
}
