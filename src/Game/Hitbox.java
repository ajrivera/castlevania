package Game;

import java.awt.Color;

import Game.Core.Sprite;

public class Hitbox extends Sprite{

	int frames = 30;
	public Hitbox(String name, int x1, int y1, int x2, int y2, double angle) {
		super(name, x1, y1, x2, y2, angle, new Color(255, 0, 0, 150));
		// TODO Auto-generated constructor stub
		this.physicBody=true;
		this.terrain=false;
		this.trigger=true;
	}
	/**
	 * frame que esta pegando el golpe antes de que se elimine
	 */
	public void frame() {
		// TODO Auto-generated method stub
		frames-=1;
		if(frames==0) {
			this.delete();
			
		}
		
	}
	/**
	 * Colision del ataque con el enemigo, le quita el hp basandose en el atk del hero, si tiene escudo no se le aplica daño
	 * visionr es para comprobar si esta mirando hacia la derecha o izquierda
	 * @param enemigo
	 */
	public void colision(Enemy enemigo) {
		Hero heroS = Hero.getInstance("hero", 230, 319, 283, 380, 3.5, 20, 200, "IMG/PJCORR/untitled.gif");
		if(this.collidesWith(enemigo)){
			if(enemigo.shield) {
				
			}else {
				enemigo.hp-=heroS.atk;
			}
			System.out.println("pum "+enemigo.hp);
			if(Hero.visionr) {
				System.out.println("PUSHRIGHT");
				enemigo.addForce(0.80, -0.5);
			}else {
				System.out.println("PUSHLEFT");
				enemigo.addForce(-0.80, -0.5);
			}
//			otro.addForce(0, -0.1*otro.hp);
		}
	}
	
	

}
