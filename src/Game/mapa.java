package Game;

import java.util.ArrayList;

import Game.Core.Sprite;

public class mapa {
	ArrayList<Sprite> sprites = new ArrayList<>();
	ArrayList<Sprite> platmovil = new ArrayList<>();
	ArrayList<Sprite> plataformas = new ArrayList<>();
	ArrayList<Sprite> platobstacle = new ArrayList<>();
	ArrayList<Sprite> AV = new ArrayList<>();

	public mapa(String string, int i, int j, int k, int l, String string2) {

	}

	public mapa() {
		// TODO Auto-generated constructor stub

	}

	public void createmap() {
		sprites.clear();
		platmovil.clear();
		plataformas.clear();
		platobstacle.clear();
		AV.clear();

		// HORIZONTAL
		platforminv platf01 = new platforminv("platf01", 39, 52, 531, 118, "");
		platforminv platf02 = new platforminv("platf02", 531, 78, 620, 180, "");
		platforminv platf03 = new platforminv("platf03", 592, 17, 1204, 92, "");
		platforminv platf04 = new platforminv("platf04", 1197, 0, 1600, 52, "");
		platforminv platf05 = new platforminv("platf05", 4, 82, 47, 297, "");
		platforminv platf06 = new platforminv("platf06", 47, 299, 146, 361, "");
		platforminv platf07 = new platforminv("platf07", 146, 361, 224, 652, "");
		platforminv platf08 = new platforminv("platf08", 224, 652, 442, 718, "");
		platforminv platf09 = new platforminv("platf09", 401, 363, 562, 684, "");
		platforminv platf10 = new platforminv("platf10", 501, 324, 639, 473, "");
		platforminv platf11 = new platforminv("platf11", 579, 272, 713, 367, "");
		platforminv platf12 = new platforminv("platf12", 713, 316, 768, 336, "");
		platforminv platf13 = new platforminv("platf13", 713, 367, 1340, 446, "");
		platforminv platf14 = new platforminv("platf14", 1307, 152, 1488, 415, "");
		platforminv platf15 = new platforminv("platf15", 1253, 168, 1307, 189, "");
		platforminv platf16 = new platforminv("platf16", 818, 228, 907, 249, "");
		platforminv platf17 = new platforminv("platf17", 997, 228, 1087, 249, "");
		platforminv platf18 = new platforminv("platf18", 1178, 228, 1267, 248, "");

		// PLAT DELTA
		platfn platfm1 = new platfn("platfm1", 740, 260, 800, 285, 0, "IMG/interact/Platform.png");
		platfn platfm3 = new platfn("platfm3", 280, 500, 340, 525, 0, "IMG/interact/Platform.png");
		// testlerp platfm2 = new testlerp("platfm2", 900, 300, 1000, 360,
		// "IMG/base2.png");

		// SPIKES
		// platf1 spikesA1 = new platf1("spikesA1", 300, 360, 400, 400,
		// "IMG/tile_50.png");
		// platf1 spikesA2 = new platf1("spikesA2", 220, 996, 400, 1016,
		// "IMG/tile_50.png");

		interact spikesA1 = new interact("spikesA1", 187, 119, 435, 138, 0, "");
		interact spikesA2 = new interact("spikesA2", 226, 626, 400, 650, 0, "");
		interact spikesA3 = new interact("spikesA3", 692, 95, 851, 114, 0, "");
		interact spikesA4 = new interact("spikesA4", 902, 94, 987, 109, 0, "");
		interact spikesA5 = new interact("spikesA5", 1017, 95, 1049, 112, 0, "");
		interact spikesA6 = new interact("spikesA6", 1077, 95, 1157, 112, 0, "");
		interact spikesA7 = new interact("spikesA7", 954, 337, 985, 364, 0, "");
		interact spikesA8 = new interact("spikesA8", 1147, 338, 1166, 363, 0, "");

		// platf1 platfm2 = new platf1("platfm2", 400, 300, 500, 400,
		// "IMG/base2.png");

		plataformas.add(platf01);
		plataformas.add(platf02);
		plataformas.add(platf03);
		plataformas.add(platf04);
		plataformas.add(platf05);
		plataformas.add(platf06);
		plataformas.add(platf07);
		plataformas.add(platf08);
		plataformas.add(platf09);
		plataformas.add(platf10);
		plataformas.add(platf11);
		plataformas.add(platf12);
		plataformas.add(platf13);
		plataformas.add(platf14);
		plataformas.add(platf15);
		plataformas.add(platf16);
		plataformas.add(platf17);
		plataformas.add(platf18);

		platmovil.add(platfm1);
		// platmovil.add(platfm2);
		platmovil.add(platfm3);

		AV.add(spikesA1);
		AV.add(spikesA2);
		AV.add(spikesA3);
		AV.add(spikesA4);
		AV.add(spikesA5);
		AV.add(spikesA6);
		AV.add(spikesA7);
		AV.add(spikesA8);

	}

}
