package Game;

import java.awt.Color;
import java.lang.reflect.Array;
import java.util.ArrayList;

import java.util.HashSet;

import Game.Core.Field;
import Game.Core.Sprite;
import Game.Core.Window;

/**
 * 
 * @author Alberto Jose Rivera Barrero
 * 
 * 
 */
public class Castle {

	static Hero heroS;

	// WINDOW
	static Field f = new Field();
	static Window w = new Window(f);
//	static Window w = WindowSingleton.getWindowSingleton(f);

	// static int jumpcd = -1;
	static interact chest = new interact("chest", 47, 234, 100, 295, 0, "IMG/interact/ChestOFF.png");
	static interact altar = new interact("altar", 1324, 93, 1402, 149, 0, "IMG/interact/AltarIdle.gif");

	static Swordsman E1 = new Swordsman("E1", 1200, 675, 1250, 735, 1.5, 10, 100,
			"IMG/enemigos/goblin/GoblinIdleMirr.gif");

//	buff buff = new buff("buff", 20, 40, 50, 70, "sunny.png");
	// U & NPCS
	// POSICION inicial standard
	static Texto t2 = new Texto("t2", 50, 50, 100, 100, "T2prueba");
	static int minscrollx = 200;
	static int minscrolly = 600;
	static int maxscrollx = 500;
	static int maxscrolly = 900;

	static mapa mapa = new mapa();

	// POSICION inicial del PJ al lado del altar para comprobar checkpoints
	// static hero hero = new hero("hero", 1210, 520, 1265, 580, 3,
	// "IMG/PJ/Untitled.gif");

	// INTERACTION BOOLS
	static boolean cofre1;
	static boolean altar1;
	static boolean mov;

	// STARTING POINT

	static int var1 = 900;
	static int stx1;
	static int sty1;
	static int stx2;
	static int sty2;
	static ArrayList<Sprite> mainD = new ArrayList<>();
	static ArrayList<interact> platinter = new ArrayList<>();
	static ArrayList<Enemy> enemigos = new ArrayList<>();
	public static ArrayList<Hitbox> attacksHit = new ArrayList<Hitbox>();

	public static void main(String[] args) throws InterruptedException {

		heroS = Hero.getInstance("hero", 413, 267, 466, 320, 3.5, 30, 200, "IMG/PJCORR/untitled.gif");
		Texto t1 = new Texto("t1", 50, 0, 100, 50, "Vida " + heroS.maxhp + " / " + heroS.hp);
		Color colhp = new Color(0, 255, 0, 155);
		ColorSprite hpbar = new ColorSprite("hpbar", 155, 36, heroS.maxhp + 155, 50, 0, colhp);

		backgroundblock top1 = new backgroundblock("top1", 0, 0, 1920, 1080, "IMG/1LVL/Level01TOP.png");
		backgroundblock back1 = new backgroundblock("back1", 0, 0, 1920, 1080, "IMG/1LVL/Level01BACK.png");

		stx1 = heroS.x1;
		sty1 = heroS.y1;
		stx2 = heroS.x2;
		sty2 = heroS.y2;
//		int[] arrst = {stx1, sty1, stx2, sty2};
		// plataformas.add(platf1);
		// plataformas.add(platf2);
//		System.out.println(Swordsman.getDrops());
		// hero.setConstantForce(0, 0.2);
		// hero.MARGIN=0;

		long last_time = System.currentTimeMillis();
		mapa.createmap();

		f.background = "black.png";

		mainD.add(back1);

		enemigos.add(E1);

		int platA = 5400;
		int platpit = 8000;
		// int letest = 3000;
		int varm = 2;
		int varpp = 1;

		platinter.add(chest);
		platinter.add(altar);
		mainD.addAll(platinter);
		// mainD.add(E1);
		mainD.add(heroS);
		mainD.addAll(enemigos);
//		mainD.addAll(attacksHit);
//		mainD.addAll(mapa.sprites);
		mainD.addAll(mapa.AV);
		mainD.addAll(mapa.platmovil);
		mainD.addAll(mapa.plataformas);
//		mainD.addAll(mapa.platobstacle);

		// HUD
		mainD.add(top1);
		mainD.add(hpbar);
		mainD.add(t1);
		// starting window (No se utilizar el scroll)
//		f.scroll(0, -400);

		while (true) {
//			System.out.println(E1.hp);
//			System.out.println(attacksHit.size()+"ATTACKSHIT");
//			System.out.println(mainD.size()+"MAINDDDD");

			if (heroS.collidesWithList(mapa.AV).size() > 0) {
				instantdeath();
			}
			if (heroS.hp < 0) {
				instantdeath();
				heroS.hp = heroS.maxhp;

			}
			hpbar.x2 = heroS.hp + 155;
			t1.path = "Vida " + heroS.maxhp + " / " + heroS.hp;

			for (Hitbox h : attacksHit) {
				h.frame();
				if (h.frames == 0) {
//					attacksHit.clear();
				}
			}
			for (Enemy e : enemigos) {
				e.dead(e, mainD);
				if (e.y2 == heroS.y2) {
					behaviour(e);
				}
//				if(e.hp<=0) {
//					e.delete();
//					mainD.remove(e);
//				}
			}
//			System.out.println(heroS.maxhp);
//			System.out.println(heroS.hp);
//			System.out.println(heroS.isGrounded(f));
//			System.out.println(mainD.size());

//			System.out.println(heroS.aterra);
			long time = System.currentTimeMillis();
			// jumpcd--;
			/*
			 * sprites.clear(); sprites.add(platf1); sprites.add(platf2);
			 * sprites.add(platfn1); sprites.add(chest); sprites.add(hero);
			 * sprites.add(enemyA);
			 */

			// GRAVITY
			if (heroS.velocity[1] == 0 && heroS.isGrounded(f)/* && jumpcd<0 */) {
				// hero.getGrounded(f);
//				System.out.println("aaaa");
				heroS.aterra = true;
			} else {
				heroS.gravity();
				E1.setConstantForce(0, 0.26);
			}
			input();
//			System.out.println(heroS.velocity[0]);
//			System.out.println(heroS.force[0]);
//			System.out.println(heroS.y2);
			interactions();
//			instantdeath();

			// DEBUG LISTAS

//			System.out.println("speitres " + sprites.size() + " lista :" + sprites);
//			System.out.println(hero.x1);
//			System.out.println("platmovil " + platmovil.size() + " lista :" + platmovil);
//			System.out.println("plataformas " + plataformas.size() + " lista :" + plataformas);

			// PRINT SPRITES

			// System.out.println(sprites.size());
			f.draw(mainD);
//			System.out.println("xxxx");
			// if(hero.force[1]<0)System.out.println(hero.force[1]);
//			f.add(AV);
//			f.add(platmovil);
//			f.add(plataformas);
//			f.add(platobstacle);
// 			f.draw(spritestemp);

			// TIMER
			int delta_time = (int) (time - last_time);
			last_time = time;
			// System.out.println(delta_time);
			// System.out.println(System.nanoTime()/1000000);

			platA = delay(platA, delta_time);
			if (platA <= 0) {
				varm = -varm;
				platA = 5400;
			} else {
				mapa.platmovil.get(0).x1 += varm;
				mapa.platmovil.get(0).x2 += varm;
				if (heroS.stepsOn(mapa.platmovil.get(0)) && heroS.collidesWithList(mapa.platobstacle).size() == 0
						&& heroS.collidesWithList(mapa.plataformas).size() == 0) {
					heroS.x1 += varm;
					heroS.x2 += varm;
				}
			}

			platpit = delay(platpit, delta_time);
			if (platpit <= 0) {
				varpp = -varpp;
				platpit = 8000;
			} else {
				// System.out.println(platpit);
				mapa.platmovil.get(1).y1 -= varpp;
				mapa.platmovil.get(1).y2 -= varpp;
				if (heroS.stepsOn(mapa.platmovil.get(1)) || heroS.leftOn(mapa.platmovil.get(1))
						|| heroS.rightOn(mapa.platmovil.get(1))) {
					heroS.y1 -= varpp;
					heroS.y2 -= varpp;
				}
			}
//			heroS.moveLeft();

			// platfm2.x1 = (int) lerp2(platfm2.x1, 2000, 0.005);
			// platfm2.x2 = (int) lerp2(platfm2.x2, 2100, 0.005);

			// System.out.println(var1 = (int) lerp2(var1,1100,0.05));

			// 60fps
			Thread.sleep(20);
//			System.out.println("TUSMUERTOS");
//			System.out.println(Thread.activeCount());
			// mirar pixel dentro del field con el click del raton
			if (f.getCurrentMouseX() != -1) {
				System.out.println(f.getMouseX() + " " + f.getMouseY());
			}
			/**
			 * cuando se activa el altar se cambia el nivel, se puede hacer bypass si pulsas la L
			 */
			if (altar.open == 1 || w.getPressedKeys().contains('l')) {
				mapa.AV.clear();
				mapa.plataformas.clear();
				mapa.platmovil.clear();
				mapa.platobstacle.clear();
				mapa.sprites.clear();
				attacksHit.clear();
				enemigos.clear();
				platinter.clear();
				mainD.clear();
				f.clear();
				Castle2.main(null);
				break;
			}

		}

	}

	/*
	 * private static double lerp(double point1, double point2, double alpha) {
	 * return point1 + alpha * (point2 - point1); }
	 */
	/**
	 * Proba de interpolació entre dos punts amb una velocitat per a moure suau un
	 * sprite de un punt a un altre i vagi decelerant quan s'aproxima als punts
	 * desti.
	 * 
	 * v0 = x1 del sprite
	 * 
	 * @param v0 v1 = x1 desti del sprite
	 * @param v1 t = velocitat
	 * @param t  return de la seguent posicio del x1 tras la operacio
	 * @return
	 */
	private static double lerp2(int v0, int v1, double t) {
		// System.out.println(v0);
		return (1 - t) * v0 + t * v1;
	}
/**
 * IA del enemigo para que te persiga si te ve
 * @param e
 */
	private static void behaviour(Enemy e) { // TODO Auto-generated method stub

		if (e.x1 >= heroS.x2 && ((e.y2 <= heroS.y2 && e.y2 >= heroS.y1) || (e.y1 <= heroS.y2 && e.y1 >= heroS.y1))) {
			e.moveLeftE();
//			System.out.println("MOVELEFT");

		} else if (e.x2 <= heroS.x1
				&& ((e.y2 <= heroS.y2 && e.y2 >= heroS.y1) || (e.y1 <= heroS.y2 && e.y1 >= heroS.y1))) {
			e.moveRightE();
//			System.out.println("MOVERIGHT");
		} else {
			e.doNotMove();
//			System.out.println("TUSMUERTOS");
		}

	}

	/**
	 * Funcio per a retornar al personatge a un punt de inici quan colisioni amb
	 * instant death sprites com els spikes
	 * 
	 * @param Hero
	 * @param sty2
	 * @param stx2
	 * @param sty1
	 * @param stx1
	 */
	private static void instantdeath() {
		// TODO Auto-generated method stub

		heroS.x1 = stx1;
		heroS.y1 = sty1;
		heroS.x2 = stx2;
		heroS.y2 = sty2;

//		f.resetScroll();
//		f.scroll(0, -400);

	}

	/**
	 * Funcio on estan tots els ifs de les interaccions amb sprites tipus cofres o
	 * altars.
	 * 
	 * @param sty2
	 * @param stx2
	 * @param sty1
	 * @param stx1
	 * @param Hero
	 * @return
	 */
	private static void interactions() {
		// TODO Auto-generated method stub
		if (heroS.collidesWith(platinter.get(0)) && w.getPressedKeys().contains('e') && platinter.get(0).open == 0) {
			platinter.get(0).open = 1;
			platinter.get(0).changeImage("IMG/interact/ChestON.png");
			cofre1 = true;
			// System.out.println("firsttru");
			// mapa.sprites.add(buff);
		}
		if (heroS.collidesWith(platinter.get(1)) && w.getPressedKeys().contains('e') && platinter.get(1).open == 0) {
			platinter.get(1).open = 1;
			altar1 = true;
			stx1 = heroS.x1;
			sty1 = heroS.y1;
			stx2 = heroS.x2;
			sty2 = heroS.y2;
			platinter.get(1).changeImage("IMG/interact/AltarActive.gif");
		}

	}

	/**
	 * Funcio timer on li pasem una variable int per a restarla contra delta_time i
	 * aixi donar-li un delay al que volguem posar dins la funció a = int que li
	 * pasem, quant mes gran mes trigara
	 * 
	 * @param a b = delta time
	 * @param b return dona el int a per a fer el if despres per a fer cada "a"
	 *          milisegons una accio
	 * @return
	 */
	private static int delay(int a, int b) {
		// TODO Auto-generated method stub

		a = a - b;

		return a;
	}

	/**
	 * Funcio de input
	 * 
	 * @throws InterruptedException
	 */
	private static void input() throws InterruptedException {

		HashSet<Character> list = (HashSet<Character>) w.getPressedKeys();

		// System.out.println(list);

		if (list.contains('d')) {
//			if (heroS.isOnColumn(f)) {heroS.getSided(f);}
			if (heroS.velocity[0] > 0) {
//				f.scroll((int)-heroS.spd, 0);
//				minscrollx -= heroS.spd;
//				maxscrollx -= heroS.spd;
			}
			heroS.moveRight();

//			heroS.x1++;
//			heroS.x2++;
			// hero.changeImage("Solaire.gif");
			// if (hero.x1 < minscrollx) {
			// scroll positiu: A l'esquerra
			// has d'ajustar minscroll i maxscroll de la mateixa forma
			// }
		}
		if (list.contains('a')) {
//			if (heroS.isOnColumn(f)) {heroS.getSided(f);}
			if (heroS.velocity[0] < 0) {
//				f.scroll((int)heroS.spd, 0);
//				minscrollx += heroS.spd;
//				maxscrollx += heroS.spd;
			}
			heroS.moveLeft();

//			heroS.x1--;
//			heroS.x2--;
			// if (hero.x1 > maxscrollx) {
			// scroll positiu: A l'esquerra
			// has d'ajustar minscroll i maxscroll de la mateixa forma
			// }
		}

//		for (int i = 0; i < mainD.size(); i++) {
//			if (mainD.get(i).terrain) {
//				if (heroS.rightOn(mainD.get(i))) {
//					f.scroll((int)heroS.spd, 0);
//					heroS.x1--;
//					heroS.x2--;
//				} else if (heroS.leftOn(mainD.get(i))) {
//					f.scroll((int)-heroS.spd, 0);
//					heroS.x1++;
//					heroS.x2++;
//				}
//			}
//		}

		if (list.contains('w') /* && jumpcd<0 */) {
			// jumpcd=20;
			heroS.jump();
		}
		if (!w.getPressedKeys().contains('d') && !w.getPressedKeys().contains('a') && heroS.aterra != false) {
			heroS.doNotMove();

		} /*
			 * if (w.getKeysDown().contains("p")) {
			 * 
			 * }
			 */
		/**
		 * ataque melee
		 */
		if (w.getKeysDown().contains(' ')) {
			Hitbox h = heroS.Melee();
			h.colision(E1);
			attacksHit.add(h);

		}
		/*
		 * else if(s.size()>0){ s.remove(s.size()-1); }
		 */
		
		/**
		 * para comprobar la muerte del hero
		 */
		if (list.contains('j') /* && jumpcd<0 */) {
			// jumpcd=20;
			System.out.println("dolor" + heroS.hp);
			heroS.hp -= 10;
		}
	}

	/**
	 * Funcio per a aplicar al personatge la força de gravetat. S'acabara utilitzant
	 * per a la gravetat dels enemics tambe.
	 */

}

/**
 * IA sencilla para el enemigo, nos perseguira teniendo en cuenta que sus
 * coordenadas de un lado de nuestro personaje no sean iguales a las del suyo
 * del lado opuesto.
 * 
 */

/*
 * public static void disparos() { // TODO Auto-generated method stub // for
 * (Shoot dis : s) { s.collision(); s.frames(); //
 * Castle.sprites.remove(sprites.size()-1);
 * 
 * // } }
 */
/*
 * private static void input() throws InterruptedException {
 * 
 * HashSet<Character> list = (HashSet<Character>) w.getPressedKeys(); if
 * (list.contains('s') && hero.collidesWith(platfn1)) { platfn1.terrain = false;
 * System.out.println("platfn"); } else { platfn1.terrain = true; } if
 * (w.getKeysDown().contains(' ')) { sprites.add(s); }
 * 
 * else if(s.size()>0){ s.remove(s.size()-1); }
 * 
 * 
 * }
 */

/*
 * public static Sprite interactions() { // TODO Auto-generated method stub
 * boolean tru = false; if (hero.collidesWith(chest) &&
 * w.getPressedKeys().contains('e') && chest.open == 0) { chest.open = 1;
 * chest.changeImage("chesto.png"); System.out.println("col"); tru = true; } if
 * (tru) { return buff; } else { return null; }
 * 
 * }
 */
