package Game;

import Game.Core.Sprite;

public class buff extends Sprite {

	public buff(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		solid = false;
		terrain = false;
		physicBody = false;
		unscrollable = true;
	}
}
