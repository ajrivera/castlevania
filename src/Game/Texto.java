package Game;

import Game.Core.Sprite;

public class Texto extends Sprite {

	public Texto(String name, int x1, int y1, int x2, int y2, String path) {
		// TODO Auto-generated constructor stub
		super(name, x1, y1, x2, y2, path);
		this.text = true;
		this.unscrollable = true;
		this.textColor = 0xffffff;
	}

}
