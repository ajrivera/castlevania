package Game;

import java.util.HashMap;

public class Shielded extends Enemy {
	/**
	 * drops de l'enemic
	 */
	private static HashMap<Items, Double> drops = new HashMap<Items, Double>();

	int invframes = 300;

	public Shielded(String name, int x1, int y1, int x2, int y2, double spdde, int atk, int maxhp, String path) {
		super(name, x1, y1, x2, y2, spdde, atk, maxhp, path);
		// TODO Auto-generated constructor stub
		getDrops().put(Shields.Dshield, 40.0);
		getDrops().put(Consumable.LowHPpot, 80.0);
	}

	/**
	 * Delay de uns 5 segons alterna el personatge en tenir el shield posat o no, el
	 * que fa que sigui invulnerable
	 */
	public void frameInv() {
		// TODO Auto-generated method stub
		invframes -= 1;
//		System.out.println(invframes);
		if (invframes == 0) {
//			System.out.println(this.shield);
			this.shield = !this.shield;
			invframes = 300;
		}

	}

	public static HashMap<Items, Double> getDrops() {
		return drops;
	}

	public static void setDrops(HashMap<Items, Double> drops) {
		Shielded.drops = drops;
	}

}
