package Game;

import Game.Core.Sprite;

public class interact extends Sprite {

	/**
	 * var per a comprobar si ha obert el accionable o no
	 */
	int open;

	public interact(String name, int x1, int y1, int x2, int y2, int open, String path) {
		super(name, x1, y1, x2, y2, path);
		terrain = false;
		physicBody = false;
		trigger = true;
		this.open = open;
	}

}
