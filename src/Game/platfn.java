package Game;

import Game.Core.Sprite;

public class platfn extends Sprite {

	public platfn(String name, int x1, int y1, int x2, int y2, double angle, String path) {
		super(name, x1, y1, x2, y2, angle, path);
		trigger = false;
		terrain = true;
		physicBody = false;
		solid = true;
	}
}
