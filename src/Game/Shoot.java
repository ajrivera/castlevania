package Game;

import Game.Core.Sprite;

public class Shoot extends Sprite {
	/**
	 * var per a fer el delay dels proyectils
	 */
	int frames = 20;
	public Shoot(String name, int x1, int y1, int x2, int y2, double angle, String path) {
		super(name, x1, y1, x2, y2, angle, path);
		physicBody=false;
		trigger=true;
		
	}
	/**
	 * funcio de colisio de els proyectils, esborren l'enemic quan impacta amb ell
	 */
	
//	public void collision() {
//		// TODO Auto-generated method stub
//		if(this.collidesWith(Castle.enemyA)) {
//			System.out.println("pum");
//			Castle.enemyA.delete();
//			
//		}
//	}
	/**
	 * delay per als proyectils
	 */
	public void frames() {
		// TODO Auto-generated method stub
		frames--;
		if(frames==0) {
			this.delete();
			
		}
	}

}
