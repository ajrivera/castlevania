package Game;

import java.util.HashMap;

import Game.Core.Sprite;

public class Swordsman extends Enemy {
	/**
	 * drops del enemigo
	 */
	private static HashMap<Items, Double> drops = new HashMap<Items, Double>();

	public Swordsman(String name, int x1, int y1, int x2, int y2, double spdde, int atk, int maxhp, String path) {
		super(name, x1, y1, x2, y2, spdde, atk, maxhp, path);
//		Swordsman.drops = dropsE;
		getDrops().put(Armas.Zweihander, 50.0);
		getDrops().put(Armas.Flail, 50.0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * getter
	 * 
	 * @return
	 */
	public static HashMap<Items, Double> getDrops() {
		return drops;
	}

	/**
	 * setter
	 * 
	 * @param drops
	 */
	public static void setDrops(HashMap<Items, Double> drops) {
		Swordsman.drops = drops;
	}

	/**
	 * attack de l'enemic quan s'acosta al hero --encara no hi esta implementat
	 */
	public void attack() {
		Hero heroS = Hero.getInstance("hero", 230, 319, 283, 380, 3.5, 20, 200, "IMG/PJCORR/untitled.gif");
		heroS.hp -= 10;
	}

}
